Ejercicios de Scripting para sus diferentes plataformas.


![alt tag](https://alejandrocq.files.wordpress.com/2010/04/terminal-tips.jpg)

## Lista de Ejercicios Batch
| Nombre del Ejercicio          | Descripcion   |
| ------------------------------|---------------|
| batch001_prueba               | Pide que presiones una tecla e imprime texto en consola.
| batch002_if                   | Prueba de la sentencia if, haciendo una pregunta.
| batch003_notepad              | Al ejecutarse abre el Notepad.
| batch004_apagar               | Al ejecutarse apaga el equipo.

## Lista de Ejercicios Shell
| Nombre del Ejercicio          | Descripcion   |
| ------------------------------|---------------|
| shell001_                     | 
| shell002_                     | 